#pragma once


#include <iostream>
#include <vector>
#include "headers/Graph.hpp"

class Node;

class DisjointSet {
public:
    static Node *makeSet(Node *node);

    static Node *findSet(Node *node);

    static void unionSet(Node *n1, Node *n2);

    static void link_by_rank(Node *n1, Node *n2);

    static void print(std::vector<Node *> nodes);
};


