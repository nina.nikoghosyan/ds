#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <vector>
#include <algorithm>
#include <stdexcept>
#include <iostream>

#include "headers/DisjointSet.hpp"

enum class Color {
    WHITE,
    GRAY,
    BLACK
};

class Node;

class Edge {
public:
    int label;
    Node *source;
    Node *dest;

    Edge(Node *src, Node *dest, int lbl = 1);

    Edge *reverse() const;

    bool operator<(const Edge &e) const;

    bool operator==(const Edge &rhs) const;
};

class Node {
private:
    int data;
    int start;
    int finish;
    int rank;
    Color color;
    Node *parent;
    std::vector<Edge *> inEdges;
    std::vector<Edge *> outEdges;
public:
    Node(int data, Color color = Color::WHITE);

    Color getColor() const;

    int getData() const;

    int getStart() const;

    int getFinish() const;

    Node *getParent() const;

    int getRank() const;

    std::vector<Edge *> &getInEdges();

    std::vector<Edge *> &getOutEdges();

    void setColor(Color newColor);

    void setStart(int newStart);

    void setFinish(int newFinish);

    void setParent(Node *newParent);

    void setRank(int newRank);

    bool addInEdge(Edge *edge);

    bool addOutEdge(Edge *edge);

    bool removeInEdgeByNode(const Node *otherNode);

    bool removeOutEdgeByNode(const Node *otherNode);

    void reset();

    bool operator<(const Node &t) const;

    bool operator==(const Node &rhs) const;

    void print() const;
};

class Graph {
private:
    std::vector<Node *> allNodes;
public:
    std::vector<Node *> getAllNodes() const;

    bool addNode(int node);

    bool addNode(Node *node);

    bool addEdge(Edge *edge);

    bool removeNode(Node *node);

    Node *getNodeById(int id);

    bool containsNode(const Node *node);

    void dfs();

    void dfsVisit(Node *node, int &time);

    std::vector<Node *> topologicalSort();

    std::vector<Edge *> mstKruskal();

    void print() const;

    std::vector<Edge *> getAllEdges();
};

#endif // GRAPH_HPP
