#include "headers/DisjointSet.hpp"

Node *DisjointSet::makeSet(Node* node) {
    node->setParent(node);
    return node;
}

Node *DisjointSet::findSet(Node *node) {
    if (node->getParent()->getData() != node->getData()) {
        node->setParent(findSet(node->getParent()));
    }
    return node->getParent();
}

void DisjointSet::unionSet(Node *n1, Node *n2) {
    auto v1 = findSet(n1);
    auto v2 = findSet(n2);
    if (v1->getData() != v2->getData())
        link_by_rank(v1, v2);
}

void DisjointSet::link_by_rank(Node *n1, Node *n2) {
    if (n1->getRank() > n2->getRank()) {
        n2->setParent(n1);
    } else if (n1->getRank() < n2->getRank()) {
        n1->setParent(n2);
    } else {
        n1->setParent(n2);
        n2->setRank(n2->getRank() + 1);
    }
}

void DisjointSet::print(const std::vector<Node *> nodes) {
    std::cout << std::endl << "DISJOINT SETS:" << std::endl;
    for (auto it: nodes) {
        std::cout << "Value is: " << it->getData()
                  << ", Rank is: " << it->getRank()
                  << ", Parent is: " << findSet(it)->getData()
                  << std::endl;
    }
}
