#include "headers/Graph.hpp"

//______________________________Graph______________________________

std::vector<Node *> Graph::getAllNodes() const {
    return this->allNodes;
}

bool Graph::addNode(int value) {
    Node *node = new Node(value);
    return addNode(node);
}

bool Graph::addNode(Node *node) {
    if (containsNode(node)) {
        return false;
    }
    this->allNodes.push_back(node);
    return true;
}

Node *Graph::getNodeById(int id) {
    for (auto node: allNodes) {
        if (node->getData() == id) {
            return node;
        }
    }
    return nullptr;
}

bool Graph::removeNode(Node *node) {
    auto it = std::find(this->allNodes.begin(), this->allNodes.end(), node);
    if (it == this->allNodes.end()) {
        return false;
    }

    for (auto &otherNode: this->allNodes) {
        otherNode->removeInEdgeByNode(node);
        otherNode->removeOutEdgeByNode(node);
    }

    this->allNodes.erase(it);
    return true;
}

bool Graph::addEdge(Edge *edge) {
    if (!this->containsNode(edge->source)) {
        throw std::runtime_error("No such source-vertex in this graph!");
    } else if (!this->containsNode(edge->dest)) {
        throw std::runtime_error("No such dest-vertex in this graph!");
    }
    edge->source->addOutEdge(edge);
    edge->dest->addInEdge(edge);
    return true;

}

bool Graph::containsNode(const Node *node) {
    auto it = std::find(this->allNodes.begin(), this->allNodes.end(), node);
    return it != this->allNodes.end();
}

void Graph::dfsVisit(Node *node, int &time) {
    node->setColor(Color::GRAY);
    node->setStart(time);
    time++;
    for (auto edge: node->getOutEdges()) {
        if (edge->dest->getColor() == Color::WHITE) {
            dfsVisit(edge->dest, time);
        }

    }
    node->setColor(Color::BLACK);
    node->setFinish(time);
    time++;

}

void Graph::dfs() {
    for (auto &node: allNodes) {
        node->reset();
    }

    int time = 0;
    for (auto &node: allNodes) {
        if (node->getColor() == Color::WHITE) {
            dfsVisit(node, time);
        }
    }

}

std::vector<Node *>
Graph::topologicalSort() {
    std::vector<Node *> sortedNodes;
    dfs();
    std::sort(allNodes.begin(), allNodes.end(),
              [](const Node *n1, const Node *n2) {
                  return n1->getFinish() > n2->getFinish();
              });

    for (const auto &node : allNodes) {
        sortedNodes.push_back(node);
    }

    return sortedNodes;
}

void Graph::print() const {
    for (auto &node: getAllNodes()) {
        node->print();
    }
}

std::vector<Edge *> Graph::getAllEdges() {
    auto allEdges = std::vector<Edge *>();
    for (auto node: this->allNodes) {
        allEdges.insert(allEdges.end(), node->getOutEdges().begin(), node->getOutEdges().end());
    }
    return allEdges;
}

std::vector<Edge *> Graph::mstKruskal() {

    auto mst = std::vector<Edge *>();
    for (auto node: this->allNodes) {
        DisjointSet::makeSet(node);
    }
    auto edges = this->getAllEdges();
    std::sort(edges.begin(), edges.end(), [](Edge *e1, Edge *e2) { return e1->label < e2->label; });

    for (auto edge: edges) {
        if (DisjointSet::findSet(edge->source) != DisjointSet::findSet(edge->dest)) {
            mst.push_back(edge);
            DisjointSet::unionSet(edge->source, edge->dest);
        }
    }
    return mst;
}

//______________________________Edge______________________________

Edge *Edge::reverse() const {
    return new Edge(dest, source, label);
}

bool Edge::operator<(const Edge &e) const {
    return this->label < e.label;
}

bool Edge::operator==(const Edge &rhs) const {
    return this->source == rhs.source &&
           this->dest == rhs.dest &&
           this->label == rhs.label;
}

Edge::Edge(Node *src, Node *dest, int lbl) :
        label(lbl),
        source(src),
        dest(dest) {}

//______________________________Node______________________________

Node::Node(int data, Color color) : data(data),
                                    color(color),
                                    start(0),
                                    finish(0),
                                    rank(0),
                                    parent(nullptr) {}

Color Node::getColor() const {
    return this->color;
}

int Node::getData() const {
    return this->data;
}

void Node::setColor(Color newColor) {
    this->color = newColor;
}

bool Node::operator<(const Node &t) const {
    return this->getData() < t.getData();
}

bool Node::operator==(const Node &rhs) const {
    return this->getData() == rhs.getData();
}

int Node::getStart() const {
    return this->start;
}

int Node::getFinish() const {
    return this->finish;
}

Node *Node::getParent() const {
    return this->parent;
}

int Node::getRank() const {
    return this->rank;
}

std::vector<Edge *> &Node::getInEdges() {
    return this->inEdges;
}

std::vector<Edge *> &Node::getOutEdges() {
    return this->outEdges;
}

bool Node::addInEdge(Edge *edge) {
    this->inEdges.push_back(edge);
    return true;
}

bool Node::addOutEdge(Edge *edge) {
    this->outEdges.push_back(edge);
    return true;
}

bool Node::removeInEdgeByNode(const Node *otherNode) {
    return false;
}

bool Node::removeOutEdgeByNode(const Node *otherNode) {
    return false;
}

void Node::setStart(int newStart) {
    this->start = newStart;

}

void Node::setFinish(int newFinish) {
    this->finish = newFinish;
}

void Node::setParent(Node *newParent) {
    this->parent = newParent;

}

void Node::setRank(int newRank) {
    this->rank = newRank;
}


void Node::reset() {
    this->color = Color::WHITE;
    this->start = 0;
    this->finish = 0;
}

void Node::print() const {
    std::cout
            << " Data:" << this->data
            << " Start:" << this->start
            << " Finish:" << this->finish
            << std::endl;
}




