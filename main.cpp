#include "headers/Graph.hpp"
#include <iostream>

int main() {
    Graph graph;
    Node *node1 = new Node(1);
    Node *node2 = new Node(2);
    Node *node3 = new Node(3);
    Node *node4 = new Node(4);
//    Node *node5 = new Node(5);
//    Node *node6 = new Node(6);
//    Node *node7 = new Node(7);
//    Node *node8 = new Node(8);
//    Node *node9 = new Node(9);

    graph.addNode(node1);
    graph.addNode(node2);
    graph.addNode(node3);
    graph.addNode(node4);
//    graph.addNode(node5);
//    graph.addNode(node6);
//    graph.addNode(node7);
//    graph.addNode(node8);
//    graph.addNode(node9);

    graph.addEdge(new Edge(node1, node2, 1));
    graph.addEdge(new Edge(node2, node1, 1));

    graph.addEdge(new Edge(node1, node4, 1));
    graph.addEdge(new Edge(node4, node1, 1));

    graph.addEdge(new Edge(node3, node4, 1));
    graph.addEdge(new Edge(node4, node3, 1));

    graph.addEdge(new Edge(node1, node3, 5));
    graph.addEdge(new Edge(node3, node1, 5));

    graph.addEdge(new Edge(node2, node3, 5));
    graph.addEdge(new Edge(node3, node2, 5));

    graph.addEdge(new Edge(node2, node4, 5));
    graph.addEdge(new Edge(node4, node2, 5));

    graph.dfs();

    graph.print();

    for (auto edge: graph.mstKruskal()) {
        std::cout << edge->source->getData() << " -> " << edge->dest->getData() << "\n";
    }

    return 0;
}
