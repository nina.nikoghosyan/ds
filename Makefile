progname=graph
CXX=g++
CXXFLAGS=-Wall -Wextra  -std=c++11 -I.
BUILDS=builds
HEADER_DIR=headers
SOURCE_DIR=sources
TEST_DIR=tests

ifeq ($(MAKECMDGOALS),)
    BUILD_DIR:=$(BUILDS)/debug
else
    BUILD_DIR:=$(BUILDS)/$(MAKECMDGOALS)
endif

debug:   CXXFLAGS+=-g3
release: CXXFLAGS+=-g0 -DNDEBUG

SOURCES:=main.cpp $(wildcard $(SOURCE_DIR)/*.cpp)
DEPENDS:=$(patsubst %.cpp, $(BUILD_DIR)/%.d, $(SOURCES))
PREPROCS:=$(patsubst %.cpp, $(BUILD_DIR)/%.ii, $(SOURCES))
ASSEMBLES:=$(patsubst %.cpp, $(BUILD_DIR)/%.s, $(SOURCES))
OBJS:=$(patsubst %.cpp, $(BUILD_DIR)/%.o, $(SOURCES))

TEST_INPUTS:=$(wildcard $(TEST_DIR)/test*.input)
TESTS:=$(patsubst $(TEST_DIR)/%.input, %, $(TEST_INPUTS))

debug:   $(BUILD_DIR) qa
release: $(BUILD_DIR) $(BUILD_DIR)/$(progname)

qa: $(TESTS)

test%: $(BUILD_DIR)/$(progname)
	./$< < $(TEST_DIR)/$@.input > $(BUILD_DIR)/$(TEST_DIR)/$@.output || echo "Negative test..."
	diff $(BUILD_DIR)/$(TEST_DIR)/$@.output $(TEST_DIR)/$@.expected > /dev/null && echo PASSED || echo FAILED

$(BUILD_DIR)/$(progname): $(OBJS) | .gitignore
	$(CXX) $(CXXFLAGS)    $^ -o $@

$(BUILD_DIR)/%.ii: %.cpp
	$(CXX) $(CXXFLAGS) -E $< -o $@
	$(CXX) $(CXXFLAGS) -MT $@ -MM $< > $(patsubst %.cpp, $(BUILD_DIR)/%.d, $<)

%.s: %.ii
	$(CXX) $(CXXFLAGS) -S $< -o $@

%.o: %.s
	$(CXX) $(CXXFLAGS) -c $< -o $@

.gitignore:
	echo $(progname) > .gitignore

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)/$(SOURCE_DIR) $(BUILD_DIR)/$(TEST_DIR)

clean:
	rm -rf $(BUILDS) .gitignore

.PRECIOUS: $(PREPROCS) $(ASSEMBLES)
.SECONDARY: $(PREPROCS) $(ASSEMBLES)

sinclude $(DEPENDS)

